datalife_product_adr
====================

The product_adr module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-product_adr/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-product_adr)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
