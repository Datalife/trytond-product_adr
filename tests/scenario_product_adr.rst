====================
Product Adr Scenario
====================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules

Install product_adr::

    >>> config = activate_modules('product_adr')
